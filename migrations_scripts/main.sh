#! /bin/bash

function multimedia(){ 
    echo "Installation en cours ..."
    sudo xbps-install -Su -y
    if [[ $? != "0" ]];then
        sudo SSL_NO_VERIFY_PEER=true xbps-install -u xbps -y && sudo SSL_NO_VERIFY_PEER=true xbps-install -Su -y
    fi
    sudo xbps-install -S lxdm xorg polkit NetworkManager network-manager-applet elogind gimp pulseaudio htop neofetch vlc firefox flameshot git curl libreoffice dbus -y
    sudo ln -s /etc/sv/NetworkManager /var/service 
    cd
    read -p "Avez vous besoin de logiciel additionnel nonfree ? : " additionnel
    if [[ $additionnel == "oui" ]];then
        sudo xbps-install -S void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree -y
        read -p "Lesquels ? : " additionnal_software
        sudo xbps-install -S $additionnal_software
        if [[ $? != "0" ]];then
            git clone git://github.com/void-linux/void-packages.git
            cd void-packages
            ./xbps-src binary-bootstrap
            echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
            ./xbps-src pkg $additionnal_software
            sudo xbps-install --repository hostdir/binpkgs/nonfree $additionnal_software
        fi 
        read -p "Voulez vous ajouter le support des flatpaks ? : " flatpak
            if [[ $flatpak == "oui" ]];then
                sudo xbps-install flatpak
                flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                read -p "Souhaitez vous installer des logiciels via flatpak (si oui lesquels) : " install_flatpak
                if [[ $install_flatpak != "non" ]];then
                    flatpak install $install_flatpak
                fi
            fi
    fi 
    echo "Telegram-desktop"
    echo "Hexchat"
    read -p "Avez vous besoin de logiciel de messagerie ? : " messagerie
    if [[ $messagerie == "oui" ]];then
        read -p "Lesquels ? : " packages
        sudo xbps-install $packages
    else 
        echo "ok"
    fi
    echo "[1] Faible configuration"
    echo "[2] Moyenne configuration"
    echo "[3] Très bonne configuration configuration"
    read -p "Veuillez choisir le niveau de configuration de votre pc : " pc
    read -p "Veuillez choisir vos pilotes vidéos : " driver
    sudo xbps-install $driver
    read -p "Avez vous besoin d'une interface graphique pour gérer vos logiciels ? : " octo 
    if [[ $octo == "oui" ]];then
        sudo xbps-install octoxbps
    else 
        echo "ok"
    fi 
    if [[ $pc == "1" ]];then
        sudo xbps-install -S xfce4 xfce4-pulseaudio-plugin -y
        sudo ln -s /etc/sv/lxdm /var/service 
        sudo ln -s /etc/sv/dbus /var/service
    fi

    if [[ $pc == "2" ]];then
        sudo xbps-install -S cinnamon -y
        sudo ln -s /etc/sv/lxdm /var/service
        sudo ln -s /etc/sv/dbus /var/service
    fi 

    if [[ $pc == "3" ]];then
        sudo xbps-remove lxdm -y
        sudo xbps-install gnome -y
        sudo ln -s /etc/sv/gdm /var/service
        sudo ln -s /etc/sv/dbus /var/service
    fi
    sudo xbps-remove -Oo -y 
    echo "Done"
    exit

}


function main(){
    echo " Bienvenue dans le script de migration vers ...."
    echo " [1] Multimédia/bureautique : "
    echo " [2] Serveur"
    read -p "Veuillez choisir un profil : " profil 
    if [[ $profil == "1" ]];then
        multimedia
    fi
}
main "$@"